#HTML
Intern: Nguyen Ngo Lap

## 1. Introductions
\- 3 core language of web dev:

- HTML: Essential structure (Most important)
- CSS: Page design
- Javascript: Page behavior

\- HTML is a markup language - explaining what a content is and how it relates to other content

\- HTML is simple, consistent and easy to learn.

### Basic HTML syntax

- Content is identified by HTML tag
- It's possible to add attribute to the elements
- It's possible to nest an element within another

### The current state of HTML

**1991**: "HTML Tags" published

**1995**: HTML 2.0 - HTML 4.0 released. Browser support for HTML 4.0 specification became nearly universal. This specification is also the most stable upto now

**2004**: WHATWG formed to continue work on HTML

**2000**: Moving HTML towards XML. XHTML 1.0 released

**2007**: HTML5 created by W3C

**2009**: Drop moving HTML towards XML

**Today**: There're currently two specifications of HTML (W3C and WHATWG)

#### HTML5 by W3C

- Clear milestones
- Stable specification

#### HTML by WHATWG

- Living standard
- No version numbers
- Undergoes constant updates and revisions

#### What's different in HTML5?

- Focuses on building applications
- More of a platform for developing applications than a markup language

### HTML resources

- https://www.w3.org/
- https://www.whatwg.org/
- https://www.webplatform.org/
- https://developer.mozilla.org/

## 2. Basic page structure
```
<!DOCTYPE HTML>
<html lang="en"> <!-- Language used for the page -->
	<head>
		<meta charset="utf-8"> <!-- Character encoding -->
		<meta name="description" content="description content">
		<title>Title</title>
		...
	</head>
	<body>
		...
	</body>
</html>
```

\- DOCTYPE declaration: https://www.w3.org/QA/2002/04/valid-dtd-list.html

### Content Models
Define the type of content expected inside an element and control syntax rules such as element nesting.

### HTML5 main content models:
- Flow: almost every element can be considered to be a part of the flow
- Metadata: metatags, no-script tags, script tags, style tags, etc.
- Embedded: content that imports other resources into the document such as canvas, iFrame, video, etc.
- Interactive: button, audio, video, etc.
- Heading: h1, h2, h3, ...
- Phrasing: text, markup text, etc.
- Sectioning: defines the scope of heading and footer

**Reference:** https://www.w3.org/TR/2011/WD-html5-20110525/content-models.html

## 3. Formatting page content
\- List of XML and HTML character entity references: https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references

## 4. Structuring content
\- Sectioning Elements: <h1>...<h6>, <article>, <aside>, <nav>, <section>

\- Semantic Elements: <header, <main>, <footer>

-> Use those elements to group content into clearly defined regions, establish the relationship between those regions and make the page more readable for both people and machines.

-> Benefits:

- More accessible sites
- Better search engine results
- Make the page more consistent and easy to style

**Reference:**

- https://www.w3.org/TR/html5/
- https://www.w3.org/TR/2014/WD-aria-in-html-20140626/
- https://www.paciellogroup.com/blog/2013/02/using-wai-aria-landmarks-2013/

## 5. HTML and CSS

\- Most browsers agree on default styling

\- Cascading Style Sheets (CSS) is to overwrite the default style

## 6. HTML and Javascript

JS: A serious language that the entire applications are built off of

- Special effects
- Improving navigation
- Validating form
- Remote scripting
- etc.